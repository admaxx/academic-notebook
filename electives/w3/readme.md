---
lang: 'pl'
title: 'Nowoczesne Technologie WWW'
author: 'Jerry Sky'
keywords: 'pwr, wust, www, nowoczesne, technologie, Lauks-Dutka, Michalski, zadania, zadanie, listy, lista, laboratorium, wykład'
---

---

- [Wykład](#wykład)
- [Laboratorium](#laboratorium)

---

## Wykład

*Prowadzony przez: [dr inż. Anna Lauks-Dutka](https://cs.pwr.edu.pl/lauks/)*

---

## Laboratorium

*Prowadzone przez: [dr Marcin Michalski](https://cs.pwr.edu.pl/michalski/)*

- [Lista 1.](lab/lista-1/readme.md)

---
