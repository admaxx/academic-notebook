---
lang: 'pl'
title: 'Programowanie Współbieżne'
author: 'Jerry Sky'
keywords: 'pwr, wust, programowanie, współbieżne, Kik, Gębala, wykład, laboratorium, listy, lista, zadań, zadanie, zadania, notatki'
---

---

- [Wykład](#wykład)
- [Laboratorium](#laboratorium)

---

## Wykład

*Prowadzony przez: [dr Marcin Kik](https://cs.pwr.edu.pl/kik/)*

---

## Laboratorium

*Prowadzone przez: [dr Maciej Gębala](https://cs.pwr.edu.pl/gebala/)*

---
