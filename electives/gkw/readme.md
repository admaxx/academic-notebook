---
lang: 'pl'
title: 'Grafika Komputerowa i Wizualizacje'
author: 'Jerry Sky'
keywords: 'pwr, wust, grafika, komputerowa, wizualizacje, Kik, Rałowski, wykład, ćwiczenia, laboratorium, notatki, zadania, zadanie, listy, lista, ćwiczeń'
---

---

- [Wykład](#wykład)
- [Ćwiczenia](#ćwiczenia)
- [Laboratorium](#laboratorium)

---

## Wykład

*Prowadzony przez: [dr Marcin Kik](https://cs.pwr.edu.pl/kik/)*

---

## Ćwiczenia

*Prowadzone przez: [dr hab. inż. Robert Rałowski](https://cs.pwr.edu.pl/ralowski/)*

- [Lista 1.](cw/lista-1/lista-1.md)

---

## Laboratorium

*Prowadzone przez: [dr Marcin Kik](https://cs.pwr.edu.pl/kik/)*

---
