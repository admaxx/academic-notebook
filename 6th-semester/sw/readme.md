---
lang: 'pl'
title: 'Systemy Wbudowane'
author: 'Jerry Sky'
keywords: 'pwr, wust, sw, systemy, wbudowane, laboratorium, wykład, Błaśkiewicz, Kozieł, lista, listy, zadań, zadanie, notatki'
---

---

- [Wykład](#wykład)
- [Laboratorium](#laboratorium)

---

## Wykład

*Prowadzony przez: [dr inż. Przemysław Błaśkiewicz](https://cs.pwr.edu.pl/blaskiewicz/)*

---

## Laboratorium

*Prowadzone przez: [mgr. inż. Patryk Kozieł](https://cs.pwr.edu.pl/koziel/)*

- [Lista 1](lab/lista-1/readme.md)

---
