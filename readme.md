---
lang: 'en-GB'
title: 'Academic Notebook'
author: 'Jerry Sky'
description: 'This notebook contains notes, programs and snippets of code related to studies of Computer Science B.Eng. at Wrocław University of Science and Technology.'
keywords: 'pwr, wppt, academic, notebook, computer science, cs, polish, pwr, python, latex, markdown'
---

---

View this repository on [the web](https://academic.jerry-sky.me) or on [GitHub](https://github.com/jerry-sky/academic-notebook).

This repository is licenced under [the *un-licence* licence](licence.md).

---

Please note:

- The main language of this repository is not English.
- There can be some hybrid multilingual expressions throughout the notes.
- As this notebook contains notes for studies in *Poland*, most of these notes are in *Polish*.

---

**Table of contents:**

- [6th Semester](#6th-semester)
    - [Core courses](#core-courses)
    - [Elective courses](#elective-courses)
- [5th Semester](#5th-semester)
    - [Core courses](#core-courses-1)
    - [Elective courses](#elective-courses-1)
- [4th Semester](#4th-semester)
    - [Core courses](#core-courses-2)
    - [Elective courses](#elective-courses-2)
    - [*Humanities*](#humanities)
- [3rd Semester](#3rd-semester)
    - [Core courses](#core-courses-3)
    - [Elective courses](#elective-courses-3)
- [2nd Semester](#2nd-semester)
    - [Core courses](#core-courses-4)
    - [Elective courses](#elective-courses-4)
- [1st Semester](#1st-semester)

---

## 6th Semester

### Core courses

- [Systemy wbudowane \(Embedded Systems\)](6th-semester/sw/readme.md)

### Elective courses

- [Nowoczesne technologie WWW \(Modern web technologies\)](electives/w3/readme.md)
- [Aplikacje mobilne \(Mobile apps\)](electives/apm/readme.md)
- [Grafika komputerowa i wizualizacje \(Computer graphics nad *computer visualisations*\)](electives/gkw/readme.md)
- [Programowanie współbieżne \(Parallel *programming*\)](electives/pw/readme.md)

---

## 5th Semester

### Core courses

- [Języki Formalne i Techniki Translacji (Automata Theory and Compilers \[lit. *Formal Languages and Translation Techniques*\])](5th-semester/jftt/readme.md)
- [Obliczenia Naukowe (Scientific Calculations — Numeric Analysis and Computing)](5th-semester/on/readme.md)

### Elective courses

- [Teoria Informacji (Information Theory)](electives/ti/readme.md)
- [Wprowadzenie do Kombinatoryki Analitycznej (Introduction to *Analytic Combinatorics*)](electives/wdka/readme.md)
- [Środowisko Programisty (Programmer’s Environment)](electives/sp/readme.md)

---

## 4th Semester

### Core courses

- [Algorytmy i struktury danych (Algorithms and Data Structures)](4th-semester/aisd/readme.md)
- [Technologie Sieciowe (Web technologies)](4th-semester/ts/readme.md)

### Elective courses

- [Wstęp do Teorii Grafów (Introduction to Graph Theory)](electives/wtg/readme.md)
- [Algorytmy Metaheurystyczne (Metaheuristic Algorithms)](electives/amh/readme.md)
- [Kodowanie i kompresja danych (Coding and Data Compression)](electives/kkd/readme.md)
- [Kurs wybranego języka programowania – Python (Programming course – Python)](electives/py/readme.md)

### *Humanities*

- [Komunikacja Społeczna (Social Communication)](humanities/ks/readme.md)
- [Przedsiębiorczość (Entrepreneurship)](humanities/pb/readme.md)

---

## 3rd Semester

### Core courses

- [Architektura komputerów i systemów operacyjnych (Computer Architecture and Operation Systems)](3rd-semester/akiso/readme.md)
- [Bazy Danych i Systemy Informacyjne (Data Bases and *Information Systems*)](3rd-semester/bdsi/readme.md)
- Technologia programowania *\(Programming technologies\)*
- Metody probabilistyczne i statystyka \(Probabilistic methods and statistics\)

### Elective courses

- Języki obce \(Foreign language\) — German B1

---

## 2nd Semester

### Core courses

- Problemy prawne informatyki \(Legal issues in IT\)
- Fizyka \(Physics\)
- Kurs programowania \(Programming course\)
- Matematyka dyskretna \(Discrete mathematics\)
- Analiza matematyczna 2 \(Calculus 2\)
- Algebra abstrakcyjna i kodowanie \(Algebra 2, Abstract algebra, Coding\)

### Elective courses

- Języki obce \(Foreign language\) — English C1

---

## 1st Semester

- Wstęp do informatyki i programowania \(Introduction to IT and programming\)
- Analiza matematyczna 1 \(Calculus 1\)
- Algebra z geometrią analityczną \(Algebra 1, Analytical geometry\)
- Logika i struktury formalne \(Mathematical logic and formal structures\)

---
